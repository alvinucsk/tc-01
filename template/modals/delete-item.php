<!-- Modal -->
<div class="modal fade" id="deleteItemModal" tabindex="-1" role="dialog" aria-labelledby="deleteItemLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteItemLabel">Delete Entry</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete <span id="entry_delete_name"></span></p>
        <form id="" action="process.php" method="POST">
            <div class="form-group">
                <input type="hidden" name="process_name" value="delete-item" />
                <input type="hidden" name="entry_id" value="" id="entry_delete_id" />
            </div>
            <button type="submit" class="btn btn-danger">Delete</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            
        </form>
      </div>
    </div>
  </div>
</div>