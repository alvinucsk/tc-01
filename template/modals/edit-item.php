<!-- Modal -->
<div class="modal fade" id="editItemModal" tabindex="-1" role="dialog" aria-labelledby="editItemLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editItemLabel">Edit Entry</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Please fill in the required fields</p>
        <form id="" action="process.php" method="POST">
            <div class="form-group">
                <input type="hidden" name="process_name" value="edit-item" />
                <input type="hidden" name="entry_id" value="" id="entry_id" />
                <label>Name<span class="text-danger">*</span></label>
                <input type="text" class="form-control" required name="name" id="edit_name" value=""/>
            </div>
            <div class="form-group">
                <label>First Name<span class="text-danger">*</span></label>
                <input type="text" class="form-control" required name="first_name"  id="edit_first_name" value=""/>
            </div>
            <div class="form-group">
                <label>Street<span class="text-danger">*</span></label>
                <input type="text" class="form-control" required name="street"  id="edit_street" value=""/>
            </div>
            <div class="form-group">
                <label>Zip-code<span class="text-danger">*</span></label>
                <input type="text" class="form-control" required name="zipcode"  id="edit_zipcode" value=""/>
            </div>
            <div class="form-group">
                <label>City<span class="text-danger">*</span></label>
                <select class="form-control" name="city_id" required  id="edit_city_id">
                    <option>Select City</option>
                    <?php
                        foreach($cities as $city) {
                            echo "<option value='".$city['id']."'>".$city['name']."</option>";
                        }
                    ?>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Save changes</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            
        </form>
      </div>
    </div>
  </div>
</div>