<!-- Modal -->
<div class="modal fade" id="addItemModal" tabindex="-1" role="dialog" aria-labelledby="addItemLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addItemLabel">Add Entry</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Please fill in the required fields</p>
        <form id="" action="process.php" method="POST">
            <div class="form-group">
                <input type="hidden" name="process_name" value="add-item" />
                <label>Name<span class="text-danger">*</span></label>
                <input type="text" class="form-control" required name="name" />
            </div>
            <div class="form-group">
                <label>First Name<span class="text-danger">*</span></label>
                <input type="text" class="form-control" required name="first_name" />
            </div>
            <div class="form-group">
                <label>Street<span class="text-danger">*</span></label>
                <input type="text" class="form-control" required name="street" />
            </div>
            <div class="form-group">
                <label>Zip-code<span class="text-danger">*</span></label>
                <input type="text" class="form-control" required name="zipcode" />
            </div>
            <div class="form-group">
                <label>City<span class="text-danger">*</span></label>
                <select class="form-control" name="city_id" required>
                    <option>Select City</option>
                    <?php
                        foreach($cities as $city) {
                            echo "<option value='".$city['id']."'>".$city['name']."</option>";
                        }
                    ?>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Save changes</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            
        </form>
      </div>
    </div>
  </div>
</div>