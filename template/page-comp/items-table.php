<table class="table table-striped">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>First Name</th>
            <th>Street</th>
            <th>Zip Code</th>
            <th>City</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
            while($entry = $res_entries->fetch_assoc()) {
                ?>
        <tr>
            <td><?=$entry['id']?></td>
            <td><?=$entry['name']?></td>
            <td><?=$entry['first_name']?></td>
            <td><?=$entry['street']?></td>
            <td><?=$entry['zipcode']?></td>
            <td>
                <?php
                    // echo findCity($entry['city_id'], $res_cities);
                    foreach($cities as $city) {
                        if($city['id'] == $entry['city_id']) {
                            echo "<label class='badge badge-info'>".$city['name']."</label>";
                        }
                    }
                ?>
                
            </td>
            <td>
                <button class="btn btn-sm btn-primary edit_entry" 
                    data-entry-id="<?=$entry['id']?>"
                    data-entry-name="<?=$entry['name']?>"
                    data-entry-first-name="<?=$entry['first_name']?>"
                    data-entry-street="<?=$entry['street']?>"
                    data-entry-zipcode="<?=$entry['zipcode']?>"
                    data-entry-city-id="<?=$entry['city_id']?>"
                ><i class="fa fa-edit"></i> Edit</button>
                <button class="btn btn-sm btn-danger delete_entry" data-entry-id="<?=$entry['id']?>" data-entry-name="<?=$entry['name']?>"><i class="fa fa-trash"></i> Delete</button>
            </td>
        </tr>
                <?php
            }
        ?>
   
    </tbody>
</table>