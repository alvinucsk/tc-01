<?php
    $page = "home";
    
    include "process.php";
    include "template/header.php";
?>
<section class="page <?=$page?>-page">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <!-- Navbar content -->
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="export.php">Export</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="row">
            <div class="col-3">
                <!-- SideBar Section -->
                <?php
                    include "template/page-comp/sidebar.php";
                ?>
            </div>
            <div class="col-9">
                <div class="clearfix">&nbsp;</div>
                <?php
                    if(isset($_GET['status']) && $_GET['status']) {
                        if(isset($_GET['action']) && $_GET['action'] && $_GET['action'] == 'add') {
                ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Entry Added Successfully!</strong> Please check the item on the table below.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php
                        }
                        if(isset($_GET['action']) && $_GET['action'] && $_GET['action'] == 'edit') {
                ?>
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    <strong>Entry Updated Successfully!</strong> Please check the item on the table below.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php 
                        }
                        if(isset($_GET['action']) && $_GET['action'] && $_GET['action'] == 'delete') {
                ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Entry Deleted Successfully!</strong> Please check the item on the table below.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php 
                        }
                
                    }
                ?>
                <h3>Welcome to Address Book</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                
                
                <?php
                    include "template/page-comp/items-table.php";
                ?>
            </div>
            
        </div>
    </div>
</section>
<?php
    /**
     * Modals
     */
    include "template/modals/add-item.php";
    include "template/modals/edit-item.php";
    include "template/modals/delete-item.php";
?>
<?php

    include "template/footer.php";
?>