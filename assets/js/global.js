$(document).ready(function () {
    $(".edit_entry").click(function(e) {
        console.log("Entry Edit Form Triggerd");
        // Populate needed Fields
        $("#entry_id").val($(this).attr("data-entry-id"));
        $("#edit_name").val($(this).attr("data-entry-name"));
        $("#edit_first_name").val($(this).attr("data-entry-first-name"));
        $("#edit_street").val($(this).attr("data-entry-street"));
        $("#edit_zipcode").val($(this).attr("data-entry-zipcode"));
        $("#edit_city_id").val($(this).attr("data-entry-city-id"));
        $('#editItemModal').modal('show');
    });
    $(".delete_entry").click(function (e) {
        console.log("Entry Delete Form Triggerd");
        // Populate needed Fields
        $("#entry_delete_id").val($(this).attr("data-entry-id"));
        $("#entry_delete_name").html($(this).attr("data-entry-name"));
        $('#deleteItemModal').modal('show');
    });
});