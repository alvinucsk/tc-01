<?php
    include "connection.php";

    $res_cities = $conn->query("SELECT * FROM $db.city ORDER BY id DESC");
    $total_cities = $res_cities->num_rows;
    $cities = Array();
    while($city = $res_cities->fetch_assoc()) {
        $cities[] = $city;
    }

    // Fetch Entries order by latest
    $res_entries = $conn->query("SELECT * FROM $db.entry ORDER BY id DESC");
    $total_entries = $res_entries->num_rows;

    $output = "<?xml version=\"1.0\" ?>\n"; 
    $output .= "<schema>"; 
    $output .= "<entries>";
    while($entry = $res_entries->fetch_assoc()) {
        
        // $output .="<entry 
        //             id='".$entry['id']."' 
        //             name='".$entry['name']."' 
        //             first_name='".$entry['first_name']."' 
        //             street='".$entry['street']."' 
        //             zipcode='".$entry['zipcode']."'";
        // foreach($cities as $city) {
        //     if($city['id'] == $entry['city_id']) {
        //         $output .="city_id=".$city['name'];
        //     }
        // }
        // $output .= " />";
        $output .= "<entry id='".$entry['id']."'>";
        $output .= "<name>".$entry['name']."</name>";
        $output .= "<first_name>".$entry['first_name']."</first_name>";
        $output .= "<street>".$entry['first_name']."</street>";
        $output .= "<zipcode>".$entry['zipcode']."</zipcode>";
        $output .= "</entry>";
            
    }
    $output .= "</entries>";
    $output .= "</schema>"; 

    header("Content-type: text/xml"); 
    header('Content-Disposition: attachment; filename="entries-'.time().'.xml"');
    echo $output;
    
    $conn->close();
?>