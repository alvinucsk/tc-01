-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2018 at 05:10 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_case_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `date_created`, `date_modified`) VALUES
(1, 'New Christyfurt', '1994-11-22 04:52:12', '2016-01-06 18:54:44'),
(2, 'Swaniawskifurt', '1984-04-27 05:39:55', '1980-01-01 06:13:08'),
(3, 'Albertoberg', '1972-02-23 03:02:12', '2014-12-14 20:59:29'),
(4, 'Port Ova', '1977-06-14 04:27:42', '1978-04-25 18:10:04'),
(5, 'Lindsayland', '1985-09-29 14:31:42', '2004-11-24 09:19:23'),
(6, 'New Vicky', '2016-02-21 19:35:03', '2016-04-11 16:30:50'),
(7, 'New Roderick', '2000-10-01 21:51:09', '1993-06-01 19:33:32'),
(8, 'Kozeystad', '1972-01-21 00:12:58', '1983-10-22 06:46:30'),
(9, 'Suzanneport', '2015-03-02 06:29:20', '2018-03-24 06:01:20'),
(10, 'Kerlukeburgh', '2014-11-06 11:43:30', '2013-10-21 18:48:37'),
(11, 'South Victoria', '2012-04-21 09:00:50', '1970-02-16 10:59:11'),
(12, 'Albertostad', '1981-04-02 14:30:34', '1979-11-22 10:48:44'),
(13, 'South Arifurt', '1981-06-15 21:47:26', '1970-11-10 04:01:03'),
(14, 'Labadieburgh', '1996-06-02 07:09:33', '1987-12-19 09:15:37'),
(15, 'East Brendonton', '1995-10-16 12:57:19', '2006-09-08 02:54:24'),
(16, 'Lake Rupertville', '2000-07-11 12:48:00', '1981-12-19 12:47:45'),
(17, 'Rutherfordfurt', '2017-12-17 23:57:55', '2017-02-03 16:36:01'),
(18, 'Port Taniaside', '1997-07-09 18:00:31', '2010-11-24 22:46:11'),
(19, 'Lake Berniece', '2001-10-24 19:06:56', '2009-11-20 18:09:17'),
(20, 'Raynorton', '1989-06-12 13:30:18', '1997-06-19 18:53:19'),
(21, 'West Earlene', '2011-02-02 16:52:02', '1981-12-02 19:06:45'),
(22, 'Kellieshire', '1986-09-17 11:01:08', '1993-01-22 01:00:17'),
(23, 'Port Anya', '2010-07-04 22:28:49', '2016-06-14 11:54:36'),
(24, 'Lake Arturo', '1981-01-14 05:10:36', '1995-07-11 05:21:57'),
(25, 'Estaborough', '1978-11-06 20:52:37', '2016-12-25 15:25:47'),
(26, 'Cristchester', '2003-06-10 04:42:19', '2000-09-01 21:24:25'),
(27, 'New Fletcher', '1979-11-07 09:10:54', '2016-03-25 02:52:54'),
(28, 'Abshirehaven', '1971-03-22 15:14:06', '1982-11-13 20:30:44'),
(29, 'New Terrystad', '2001-07-29 04:30:21', '1976-02-22 03:33:58'),
(30, 'East Hymanchester', '1984-04-24 11:54:28', '2004-08-07 21:50:46');

-- --------------------------------------------------------

--
-- Table structure for table `entry`
--

CREATE TABLE `entry` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `street` varchar(250) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `city_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entry`
--

INSERT INTO `entry` (`id`, `name`, `first_name`, `street`, `zipcode`, `city_id`, `date_created`, `date_modified`) VALUES
(1, 'Comahig', 'Alvin', 'Escario', '6000', 15, '2018-06-26 10:50:14', '0000-00-00 00:00:00'),
(2, 'Comahig', 'Alvin', 'Escario', '6000', 29, '2018-06-26 10:51:28', '0000-00-00 00:00:00'),
(3, 'Comahig', 'Alvin', 'Escario', '6000', 29, '2018-06-26 10:51:38', '0000-00-00 00:00:00'),
(4, 'Comahig', 'Alvin', 'Escario', '6000', 29, '2018-06-26 10:51:53', '0000-00-00 00:00:00'),
(5, 'Comahig 2', 'Alvin 2', 'Escario 2', '6000', 13, '2018-06-26 10:53:25', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entry`
--
ALTER TABLE `entry`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `entry`
--
ALTER TABLE `entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
