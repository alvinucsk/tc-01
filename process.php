<?php
    /*
        Fetch Items From Database
    */
    // Fetch Cities
    include "connection.php";
    $res_cities = $conn->query("SELECT * FROM $db.city ORDER BY id DESC");
    $total_cities = $res_cities->num_rows;
    $cities = Array();
    while($city = $res_cities->fetch_assoc()) {
        $cities[] = $city;
    }

    // Fetch Entries order by latest
    $res_entries = $conn->query("SELECT * FROM $db.entry ORDER BY id DESC");
    $total_entries = $res_entries->num_rows;

    if(isset($_POST['process_name']) && $_POST['process_name'] != null) {
        switch($_POST['process_name']) {
            case 'add-item':
                $sql = "INSERT INTO `$db`.`entry` (`name`, `first_name`, `street`, `zipcode`, `city_id`)
                        VALUES (
                            '".$_POST['name']."', 
                            '".$_POST['first_name']."', 
                            '".$_POST['street']."',
                            '".$_POST['zipcode']."',
                            ".$_POST['city_id']."
                        )";
                $conn->query($sql);
                header("Location: index.php?action=add&status=1");
            break;
            case 'edit-item':
                $sql = "UPDATE `$db`.`entry` SET 
                        `name`='".$_POST['name']."', 
                        `first_name`='".$_POST['first_name']."', 
                        `zipcode`='".$_POST['zipcode']."',
                        `city_id`=".$_POST['city_id']." 
                        WHERE `id`=".$_POST['entry_id'];
                $conn->query($sql);
                header("Location: index.php?action=edit&status=1");
            break;
            case 'delete-item':
                $sql = "DELETE FROM `$db`.`entry` WHERE `id`=".$_POST['entry_id'];
                $conn->query($sql);
                header("Location: index.php?action=delete&status=1");
            break;
        }
    }
?>